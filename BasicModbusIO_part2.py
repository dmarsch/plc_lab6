#!/usr/bin/env python

#
# Based on Pymodbus Synchronous Client Example.
# Now barely any of the original remains.
#
# Does memory I/O operations with the Schneider TM221C16R in the PLC lab.
#
# B.Noble 20211028
#


#
# import the client implementation
#
from pymodbus.client.sync import ModbusTcpClient as ModbusClient

#
# configure the client logging
#
import logging
FORMAT = ('%(asctime)-15s %(threadName)-15s '
          '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

import time

def get_adc(temp):
    m = (818 - 204)/(90 - 50) #818 & 204 are ADC for 90F (8VDC) & 50F (2VDC)
    b = 204 - (m * 50)
    y = m * temp + b
    return int(y)
   
def get_fahrenheit(adc): #want to make adc value a float when passing in argument
    m = (90 - 50)/(818 - 204)
    b = 50 - (m * 204)
    y = (m * adc) + b
    return y

def f_to_c(f_temp):
    return  (f_temp - 32.0) * (5.0/9.0)

def f_to_k(f_temp):
    return 273.5 + ((f_temp - 32.0) * (5.0/9.0))

#
# Modbus unit to talk to
#
MODBUS_ADDR = "192.168.8.203" # IP address of PLC
MODBUS_PORT = 502

def run_sync_client():

    bool2binary = lambda a : 1 if a else 0

    count = 0

    #
    # Connect to the PLC
    #
    client = ModbusClient(MODBUS_ADDR, port=MODBUS_PORT)
    client.connect()
    log.info(client)

    #
    # Print an info message
    #
    log.info("Assert %I0.0 on PLC to exit this program.")

    #
    # Put our addresses in variables so we can reference them by a meaningful name
    #
    M0ADDR = 0;
    M1ADDR = 1;
    M2ADDR = 2;
    M3ADDR = 3;
    M4ADDR = 4;
    M5ADDR = 5;
    M6ADDR = 6;
    M7ADDR = 7;
    M8ADDR = 8;
    M9ADDR = 9;
    MW0ADDR = 0;
    MW1ADDR = 1;
    MW2ADDR = 2;
    MW3ADDR = 3;
    
    temp_sel = 0
    elapsed_time = 0

    # Initialize Alarm Temperature
    alarm_threshold = float(85) #this should be a float
    alarm_adc_threshold = get_adc(alarm_threshold)
    rr = client.write_register(MW0ADDR, alarm_adc_threshold) #write 85F ADC as initial value
    assert not rr.isError()

    time.sleep(1)

    while True:
        
        # start = time.time()
        while True:
                rr = client.read_coils(M9ADDR, 1) #read temperature select button
                assert not rr.isError()
                if rr.bits[0]:
                    temp_sel += 1
                    if (temp_sel % 3 == 0): #go back to Fahrenheit
                        temp_sel = 0
                    break
                else:
                    # log.info("temperature select hasn't changed")
                    break
                
        # if (elapsed_time >= 1):
        #     elapsed_time = 0 #clear this
        while True:
            rr = client.read_coils(M1ADDR, 1) #read high temp alarm coil
            assert not rr.isError()
            if rr.bits[0]:
                log.info("HIGH TEMP ALERT")
                break
            else:
                log.info("temperatures are normal")
                break
            
        #
        # Read the values of the output register holding the ADC value
        #
        log.info(f"Reading ADC value from %MW2")
        rr = client.read_holding_registers(MW2ADDR, 1)
        assert not rr.isError()
        adc_value = rr.registers[0]
        log.info(f"ADC value = {adc_value}")
        temp_value_f = get_fahrenheit(float(adc_value)) #get the F of the potentiometer ADC
        temp_value_c = f_to_c(float(temp_value_f)) #convert to C
        temp_value_k = f_to_k(float(temp_value_f)) #convert to K
        temp_val_tuple = [temp_value_f, temp_value_c, temp_value_k] #create tuple to increment through
        temp_units = [u'\N{DEGREE SIGN}F', u'\N{DEGREE SIGN}C', 'K'] #define units
        log.info(f"Temperature value = {temp_val_tuple[temp_sel]}{temp_units[temp_sel]}") #display correct conversion

        # stop = time.time()
        # elapsed_time += (stop - start) #add the time
        # #
        # # Wait one second
        # #
        time.sleep(1)
    #
    # close the client
    #
    client.close()
    log.info(f"Exiting")


if __name__ == "__main__":
    run_sync_client()
