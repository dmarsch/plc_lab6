#!/usr/bin/env python

#
# Based on Pymodbus Synchronous Client Example.
# Now barely any of the original remains.
#
# Does memory I/O operations with the Schneider TM221C16R in the PLC lab.
#
# B.Noble 20211028
#


#
# import the client implementation
#
from pymodbus.client.sync import ModbusTcpClient as ModbusClient
# from pymodbus.client.sync import ModbusUdpClient as ModbusClient
# from pymodbus.client.sync import ModbusSerialClient as ModbusClient

#
# configure the client logging
#
import logging
FORMAT = ('%(asctime)-15s %(threadName)-15s '
          '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
logging.basicConfig(format=FORMAT)
log = logging.getLogger()
log.setLevel(logging.INFO)

import time

#
# Modbus unit to talk to
#
MODBUS_ADDR = "192.168.8.203" # IP address of PLC
MODBUS_PORT = 502

def run_sync_client():

    bool2binary = lambda a : 1 if a else 0

    count = 0

    #
    # Connect to the PLC
    #
    client = ModbusClient(MODBUS_ADDR, port=MODBUS_PORT)
    client.connect()
    log.info(client)

    #
    # Print an info message
    #
    log.info("Assert %I0.0 on PLC to exit this program.")

    #
    # Put our addresses in variables so we can reference them by a meaningful name
    #
    M0ADDR = 0;
    M1ADDR = 1;
    M2ADDR = 2;
    M3ADDR = 3;
    M4ADDR = 4;
    M5ADDR = 5;
    M6ADDR = 6;
    M7ADDR = 7;
    M8ADDR = 8;
    MW0ADDR = 0;
    MW1ADDR = 1;
    MW2ADDR = 2;
    MW3ADDR = 3;

    #
    # Initialize %M0
    #
    rq = client.write_coil(M0ADDR, 0)
    assert not rq.isError()

    #
    # Initialize %M1 through %M4, read them back and display them, then sleep for 1 second
    #
    bits = [1, 0, 0]
    rq = client.write_coils(M1ADDR, bits)
    assert not rq.isError()
    rr = client.read_coils(M1ADDR, 3)
    assert not rr.isError()
    log.info(f"[%M1:%M3] = {[bool2binary(b) for b in rr.bits[0:3]]}")
    time.sleep(1)
    #
    # Loop until %M7 is set
    #
    while True:
        #
        # Read %M7; if set, assert %M0 then break out of the loop
        #
        rr = client.read_coils(M7ADDR, 1)
        assert not rr.isError()
        if rr.bits[0]:
            rq = client.write_coil(M0ADDR, 1)
            assert not rq.isError()
            log.info(f"Breaking out of loop")
            break
 
        #
        # Read/Rotate/Write the bits in %M1 through %M3
        #
        rr = client.read_coils(M1ADDR, 3)
        assert not rr.isError()
        bits = [bool2binary(b) for b in rr.bits[0:3]]
        bits = bits[-1:] + bits[0:-1]
        rq = client.write_coils(M1ADDR, bits)
        assert not rq.isError()
        rr = client.read_coils(M1ADDR, 3)
        assert not rr.isError()
        log.info(f"[%M1:%M3] = {[bool2binary(b) for b in rr.bits[0:3]]}")

        #
        # Write a floating-point Fahrenheit temperature to holding register %MW0
        #
        log.info(f"{count} -> %MW0")
        rr = client.write_register(MW0ADDR, count)
        assert not rr.isError()

        #
        # Wait until the math operation is done
        #
        while True:
            rr = client.read_coils(M8ADDR, 1)
            assert not rr.isError()
            if rr.bits[0]:
                break
            else:
                log.info("Calculation not ready")
    
        #
        # Read the values of the output registers (%QWM)
        #
        rr = client.read_holding_registers(MW1ADDR, 1)
        assert not rr.isError()
        log.info(f"%MW1 -> {rr.registers}")
    
        #
        # Read the values of the output register holding the ADC value
        #
        log.info(f"Reading ADC value from %MW2")
        rr = client.read_holding_registers(MW2ADDR, 1)
        assert not rr.isError()
        adc_value = rr.registers[0]
        log.info(f"ADC value = {adc_value}")

        #
        # Quantize the ADC value into 8 levels and show them as 3-bits on Q0.5:Q0.7
        #
        adc3q_value= adc_value >> 7
        adc3q_str = bin(adc3q_value)[2:]
        adc3q_str = ['0' for i in range(0,3-len(adc3q_str))] + list(adc3q_str)
        adc_3bits = [int(i) for i in adc3q_str]
        print(len(adc_3bits))
        rq = client.write_coils(M4ADDR, adc_3bits)
        assert not rq.isError()
        log.info(f"[%M5:%M7] = {adc_3bits}")

        #
        # Wait one second
        #
        time.sleep(1)
        
        #
        # Increment the loop count
        #
        count = count + 1

    #
    # close the client
    #
    client.close()
    log.info(f"Exiting")


if __name__ == "__main__":
    run_sync_client()
